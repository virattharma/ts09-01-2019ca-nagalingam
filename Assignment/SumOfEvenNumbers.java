package Assignment;

import java.util.*;

public class SumOfEvenNumbers {
	public static void main(String[] args) {
		int sum=0;
		int i=2;
		Scanner s1= new Scanner(System.in);
		System.out.println("Enter The Value Of N"); 
		int n=s1.nextInt();
		while(i<=n) {
			sum=sum+i;
			i=i+2;
		}
		System.out.println("Sum Of Even Numbers From 1 to "+ n +" = "+ sum); 
		
	}

}
